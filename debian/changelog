libtext-wikicreole-perl (0.07-3) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 20:35:46 +0100

libtext-wikicreole-perl (0.07-2) unstable; urgency=medium

  * Take over for the Debian Perl Group on maintainer's request
    (https://lists.debian.org/debian-perl/2017/08/msg00056.html)
  * debian/control: Changed: Homepage field changed to metacpan.org URL;
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Bernd Zeimetz
    <bzed@debian.org>)
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.1.0.
  * Drop version from perl build dependency.
  * Switch to source format "3.0 (quilt)".
  * Bump debhelper compatibility level to 9.
  * Update Vcs-* fields.
  * Use MetaCPAN URL in debian/watch.
  * debian/rules: switch to dh(1). And don't install README anymore.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Add /me to Uploaders.
  * Make short description a noun phrase.

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Sep 2017 20:19:48 +0200

libtext-wikicreole-perl (0.07-1) unstable; urgency=low

  * New Upstream Version
  * debian/control, debian/patches, debian/rules:
    - Dropping all patches, applied by upstream. Also removing quilt
      build-dependency and patch parts from debian/rules.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 05 Oct 2008 23:45:37 +0200

libtext-wikicreole-perl (0.05-2) unstable; urgency=low

  * debian/rules, debian/control, debian/README.source:
    - Adding quilt patch management system and document how to use it.

  * debian/patches:
    - Adding creole-barelink.patch to allow custom handling of "bare" links.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 22 Jun 2008 00:57:45 +0200

libtext-wikicreole-perl (0.05-1) unstable; urgency=low

  * Initial Release (Closes: #487020)

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 19 Jun 2008 10:27:55 +0200
